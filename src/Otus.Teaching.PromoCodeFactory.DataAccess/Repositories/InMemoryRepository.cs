﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> CreateAsync(T el)
        {
            if (Data.FirstOrDefault(x => x.Id == el.Id) != null)
            {
                return Task.FromResult(false);
            }
            else
            {
                Data = Data.Append(el);
                return Task.FromResult(true);
            }

        }

        public Task<bool> DeleteAsync(Guid id)
        {
            return Task.Run(() =>
            {
                if (Data.FirstOrDefault(x => x.Id == id) != null)
                {
                    Data = Data.Where(x => x.Id != id).ToList();
                    return true;
                }
                else
                {
                    return false;
                }
            });


        }

        public Task<bool> UpdateAsync(Guid id, T el)
        {
            return Task.Run(() =>
            {
                if (Data.FirstOrDefault(x => x.Id == id) != null)
                {
                    el.Id = id;
                    Data = Data.Where(x => x.Id != id).ToList();
                    Data = Data.Append(el);
                    return true;
                }
                else
                {
                    return false;
                }
            });
        }

    }
}